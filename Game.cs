﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Kinect;
using System.Collections;
using System.Windows.Forms;
using System.Media;
//using System.Timers;

namespace Microsoft.Samples.Kinect.BodyBasics
{
    public class Game
    {
        // Field
        public Boolean enemy;
        public int score;
        public DateTime currentTime;
        public ArrayList collectibles;
        public Random rnd = new Random();
        // Constructor that takes no arguments.
        public Timer gameTimer = new Timer();
        public int timeLeft = 60;
        public int passed = 0;
        public double speedMult = 0.5;
        public Boolean started = false;
        public Boolean powerUpOnBoard = false;
        public int powerUpTimer = 0;
        public Boolean penaltyOnBoard = false;
        public int penaltyTimer = 0;
        public Collectible currentPowerUp;
        public Collectible currentPenalty;
        public double sizeMultiplier = 1;
        public int sizeTimer = 0;
        public Collectible penalty;
        public int highScore = 0;
        public Boolean showEndGameSlide = false;

        public int x2Timer = 0;
        public Boolean x2 = false;
        public int scoreMultiplier = 1;
        public SoundPlayer simpleSound = new SoundPlayer(@"C:\Users\Workspace\Desktop\Hack2015\cookieCruncher\Sounds\cookie.wav");
        

        public Game()
        {
            gameTimer.Interval = 1000; // specify interval time as you want
            gameTimer.Tick += new EventHandler(timePassed);
            //   gameTimer.
            passed = 0;
            enemy = true;
            score = 0;
            currentTime = new DateTime();
            collectibles = new ArrayList();

            collectibles.Add(new Collectible("start", rnd, this));

            string text = System.IO.File.ReadAllText(@"C:\Users\Workspace\Desktop\Hack2015\cookieCruncher\database\score");
            highScore = System.Int32.Parse(text);

        }

        public void start() {
            score = 0;
            started = true;
            showEndGameSlide = false;

            collectibles.Clear();
            gameTimer.Start();

            penalty = new Collectible("penalty", rnd, this);

            collectibles.Add(new Collectible("powerup", rnd, this));
            collectibles.Add(penalty);
            collectibles.Add(new Collectible("simple", rnd, this));
            collectibles.Add(new Collectible("simple", rnd, this));
            collectibles.Add(new Collectible("simple", rnd, this));
            collectibles.Add(new Collectible("simple", rnd, this));
        }

        public void end() {
            timeLeft = 60;
            passed = 0;
            speedMult = 0.5;
            sizeMultiplier = 1;

            gameTimer.Stop();
            collectibles.Clear();
            collectibles.Add(new Collectible("start", rnd, this));
            started = false;
            if (highScore < score)
            {
                highScore = score;
                System.IO.File.WriteAllText(@"C:\Users\Workspace\Desktop\Hack2015\cookieCruncher\database\score", score.ToString());
            }
            showEndGameSlide = true;

        }
        public void timePassed(object sender, EventArgs e) {

            if (passed % 5 == 0) {
                
                collectibles.Add(new Collectible("simple", rnd, this));
                speedMult += 0.1;
            }

            if (passed % 7 == 0)
            {
                if (penalty.area.Y > 500)
                {
                int x = rnd.Next(5, 350);
                penalty.area = new Rect(x, 0, 50 * sizeMultiplier, 50 * sizeMultiplier);
                }
            }

            if (passed % 8 == 0)
            {

                powerUpOnBoard = true;
                powerUpTimer = 3;
                int powerup = rnd.Next(0, 3);

                if (powerup == 1)
                {
                    currentPowerUp = new Collectible("sizeUp", rnd, this);
                }
                else if (powerup == 2)
                {
                    currentPowerUp = new Collectible("x2", rnd, this);
                }
                else
                {
                    currentPowerUp = new Collectible("time5up", rnd, this);
                }

                 collectibles.Add(currentPowerUp);
            }

            if (passed % 14 == 0)
            {
                penaltyOnBoard = true;
                penaltyTimer = 3;
                int penalty = rnd.Next(0, 2);

                if(penalty == 1)
                {
                    currentPenalty = new Collectible("sizeDown", rnd, this);
                }
                else
                {
                    currentPenalty = new Collectible("time5down", rnd, this);
                }

                collectibles.Add(currentPenalty);
            }
            timeLeft -= 1;
            passed += 1;

            if (powerUpOnBoard && powerUpTimer == 0) {
                collectibles.Remove(currentPowerUp);
                powerUpOnBoard = false;
            }

            if (penaltyOnBoard && penaltyTimer == 0)
            {
                collectibles.Remove(currentPenalty);
                powerUpOnBoard = false;
            }

            if (sizeTimer == 0)
            {
                sizeMultiplier = 1;
                recalculateSize();
            }

            if (x2Timer == 0)
            {
                scoreMultiplier = 1;
                x2 = false;
            }

            if (powerUpOnBoard) powerUpTimer--;
            if (penaltyOnBoard) penaltyTimer--;
            if (sizeMultiplier != 1) sizeTimer--;
            if (x2) x2Timer--;
            if (timeLeft == 0) {
                gameTimer.Stop();
                end();
            }
            //score += 999999;
            //gameTimer.Stop();
        }

        internal void jointPositionCheck(Dictionary<JointType, Point> jointPoints)
        {
            /*            foreach (Collectible c in collectibles.ToArray()) {
                            if (c.area.Contains(jointPoints[JointType.HandLeft]) ||
                                c.area.Contains(jointPoints[JointType.HandRight]))
                            {
                                c.setCollected();
                            }

                           }
                           */

            foreach (Collectible c in collectibles.ToArray())
            {
                double offset = 20;
                Rect leftHandRect = new Rect(
                    new Point(jointPoints[JointType.HandLeft].X - offset, jointPoints[JointType.HandLeft].Y - offset),
                    new Point(jointPoints[JointType.HandLeft].X + offset, jointPoints[JointType.HandLeft].Y + offset)
                    );
                Rect rightHandRect = new Rect(
                    new Point(jointPoints[JointType.HandRight].X - offset, jointPoints[JointType.HandRight].Y - offset),
                    new Point(jointPoints[JointType.HandRight].X + offset, jointPoints[JointType.HandRight].Y + offset)
                    );

                if (c.area.IntersectsWith(leftHandRect)|| c.area.IntersectsWith(rightHandRect))
                {
                    c.setCollected();
                }

            }

        }

        internal void newCollectible()
        {
            collectibles.Add(new Collectible("simple", rnd, this));
        }

        internal void addScore(int v)
        {
            score += v * scoreMultiplier;
        }

        internal void removeScore(int v)
        {
            score -= v;
        }

        internal void recalculateSize()
        {
            foreach(Collectible c in collectibles)
            {
                double curr_x = c.area.X;
                double curr_y = c.area.Y;

                c.area = new Rect(curr_x, curr_y, 50 * sizeMultiplier, 50 * sizeMultiplier);
            }
        }
    }

    public class Collectible
    {
        public String type;
        public Rect area;
        public Vector vector;
        public Boolean collected;
        public Game game;
        public Collectible(String tp, Random rnd, Game g)
        {
            type = tp;
            game = g;
            //make random
            double x_speed = game.rnd.Next(-4, 4);
            double y_speed = game.rnd.Next(-4, 4);
            if (tp.Equals("simple") || tp.Equals("start"))
            {
                vector = new Vector(x_speed * game.speedMult, y_speed * game.speedMult);
            }

            int x = rnd.Next(5, 450);
            int y = rnd.Next(5, 350);
            
            area = new Rect(x, y, 50 * game.sizeMultiplier, 50 * game.sizeMultiplier);

            if (type.Equals("start"))
            {
                area = new Rect(20, 20, 50 * game.sizeMultiplier, 50 * game.sizeMultiplier);
            }
            if (type.Equals("penalty")) {
                area = new Rect(x, -30 , 50 * game.sizeMultiplier, 50 * game.sizeMultiplier);
                x_speed = game.rnd.Next(-3, 3);
                y_speed = game.rnd.Next(4, 10);
                vector = new Vector(x_speed * game.speedMult, y_speed * game.speedMult);
            }

            collected = false;
        }

        public void changePosition() {
            double x_speed = game.rnd.Next(-10, 10);
            double y_speed = game.rnd.Next(-10, 10);
            vector = new Vector(x_speed * game.speedMult, y_speed * game.speedMult);

            int x = game.rnd.Next(5, 350);
            int y = game.rnd.Next(5, 350);
            area = new Rect(x, y, 50 * game.sizeMultiplier, 50 * game.sizeMultiplier);

            if (type.Equals("penalty"))
            {
                x = game.rnd.Next(5, 550);
                area = new Rect(x, -30, 50 * game.sizeMultiplier, 50 * game.sizeMultiplier);
                x_speed = game.rnd.Next(-2, 2);
                y_speed = game.rnd.Next(4, 8);
                vector = new Vector(x_speed, y_speed);
            }

        }
        internal void setCollected()
        {
            if (type.Equals("simple")) {
                game.addScore(10);
                game.simpleSound.Play();
                this.changePosition();
            }
            else if (type.Equals("powerup")){
                game.addScore(100);
                this.changePosition();
            }
            else if (type.Equals("x2"))
            {
                game.x2Timer = 4;
                game.x2 = true;
                game.scoreMultiplier = 2;
                game.collectibles.Remove(this);
            }
            else if (type.Equals("time5up"))
            {
                game.timeLeft += 5;
                game.collectibles.Remove(this);
            }
            else if (type.Equals("time5down"))
            {
                
                if (game.timeLeft > 5)
                {
                    game.timeLeft -= 5;
                } else
                {
                    game.timeLeft = 1;
                }
                game.collectibles.Remove(this);
            }
            else if (type.Equals("sizeUp"))
            {
                game.sizeMultiplier = 2;
                game.sizeTimer = 4;
                game.recalculateSize();
                game.collectibles.Remove(this);
            }
            else if (type.Equals("sizeDown"))
            {
                game.sizeMultiplier = 0.5;
                game.sizeTimer = 4;
                game.recalculateSize();
                game.collectibles.Remove(this);
            }
            else if (type.Equals("start"))
            {
                game.start();
            }
            else
            {
                game.removeScore(100);
                this.changePosition();
            }
            
            //this.collected = true;

            //game.newCollectible();



        }

        private void elseif()
        {
            throw new NotImplementedException();
        }
    }
}


